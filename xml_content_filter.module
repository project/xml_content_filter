<?php

/**
 * @file
 * Implements a mechanism for XML based filter tags. Modules can implement
 * hook_xml_content_filter() to expose their own XML filter tags.
 *
 * This module utilizes the relaxed parser used by MediaWiki and will
 * tolerate malformed XML (unlike PHP's native XML parser).
 */


/**
 * Regular expression to match HTML/XML attribute pairs within a tag.
 * Allows some... latitude. Extracted from MediaWiki.
 */
$xml_content_filter_attrib = '[A-Za-z0-9]';
$xml_content_filter_space  = '[\x09\x0a\x0d\x20]';
define('XML_CONTENT_FILTER_ATTRIB_REGEX',
	"/(?:^|$xml_content_filter_space)($xml_content_filter_attrib+)
	  ($xml_content_filter_space*=$xml_content_filter_space*
		(?:
		 # The attribute value: quoted or alone
		  \"([^<\"]*)\"
		 | '([^<']*)'
		 |  ([a-zA-Z0-9!#$%&()*,\\-.\\/:;<>?@[\\]^_`{|}~]+)
		 |  (\#[0-9a-fA-F]+) # Technically wrong, but lots of
							 # colors are specified like this.
							 # We'll be normalizing it.
		)
	   )?(?=$xml_content_filter_space|\$)/sx");


/**
 * Implementation of hook_perm().
 */
function xml_content_filter_perm() {
  return array('view XML content filter errors');
}


/**
 * Implementation of hook_filter().
 */
function xml_content_filter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('XML filter API'));

    case 'description':
      return t('Allows modules to process XML tags as input filters.');

    case 'prepare':
      return $text;

    case 'process':
      return xml_content_filter_run_parser($text);
  }
}


/**
 * Parse the text for XML content tags, invoking any element handlers that
 * exist for the elements encountered in the text.
 *
 * @param $text string
 *  The text to parse.
 * @param $format string
 *  The target output format. This parameter is passed to each XML element
 *  handler to indicate the output format. By default, this is 'html', but
 *  elements could also be passed a value such as 'text' to indicate that
 *  the resulting output will be text only.
 *
 * @return string
 *  The parsed text.
 */
function xml_content_filter_run_parser($text, $format = 'html') {
  $tags    = module_invoke_all('xml_content_filter');
  $matches = array();
  $text    = xml_content_filter_extract(array_keys($tags), $text, $matches, "\x07UNIQ");

  foreach ($matches as $marker => $data) {
    list($element, $content, $attributes, $tag) = $data;

    // Process nested items first
    $content = xml_content_filter_run_parser($content, $format);

    $output = call_user_func($tags[$element], $content, $attributes, $format);
    $text   = str_replace($marker, $output, $text);
  }

  return $text;
}


/**
 * Implementation of hook_xml_content_filter(). This implementation
 * offers a single test element:
 *  - xml_content_filter_test
 *
 * @return array
 *  A mapping of filter element names to the names of the functions that
 *  handle them. 
 */
function xml_content_filter_xml_content_filter() {
  return array(
    'xml_content_filter_test' => 'xml_content_filter_test',
  );
}


/**
 * Handle the test filter element.
 *
 * @param $content string
 *  The text between the opening and closing tags of the element, if any.
 * @param $attributes array
 *  A mapping of element attribute names to their values.
 * @param $format string
 *  An optional parameter requesting a specific format for the returned
 *  data.
 *
 * @return string
 *  The result of processing the element and its content.
 */
function xml_content_filter_test($content, $attributes, $format = 'html') {
  return '<strong>The XML parser works.</strong>';
}


/**
 * Report an error with the XML tag's attributes. Modules implementing 
 * this filter should use this method to report any errors they
 * encounter when processing the attribute values of their XML tag.
 *
 * @param $error_msg
 *  The translated error message to display.
 */
function xml_content_filter_error($error_msg) {
  if (user_access('view XML content filter errors')) {
    drupal_set_message($error_msg, 'error');
  }

  return;
}


/**
 * Replaces all occurrences of HTML-style comments and the given tags
 * in the text with a random marker and returns the next block of text.
 * Extracted from MediaWiki.
 *
 * @param $elements array
 *  List of element names. Comments are always extracted.
 * @param $text string
 *  Source text string.
 * @param &$matches array
 *  Output from the function is stored in this array. After the function is
 *  called, this array will be an associative array filled with data in
 *  the form:
 *  <code>
 *  'UNIQ-xxxxx' => array(
 *     'element',
 *     'tag content',
 *     array( 'param' => 'x' ),
 *     '<element param="x">tag content</element>' )
 *  )
 *  </code>
 * @param $uniq_prefix string
 *  If present, this will be prefixed to the markers for the replaced
 *  elements.
 */
function xml_content_filter_extract($elements, $text, &$matches, $uniq_prefix = ''){
  static $n = 1;
  $stripped = '';
  $matches  = array();

  $taglist = implode('|', $elements);
  $start   = "/<($taglist)(\\s+[^>]*?|\\s*?)(\/?>)|<(!--)/i";

  while ('' != $text) {
    $p = preg_split($start, $text, 2, PREG_SPLIT_DELIM_CAPTURE);
    $stripped .= $p[0];
    if (count($p) < 5) {
      break;
    }
    
    if (count($p) > 5) {
      // comment
      $element    = $p[4];
      $attributes = '';
      $close      = '';
      $inside     = $p[5];
    } 
    else {
      // tag
      $element    = $p[1];
      $attributes = $p[2];
      $close      = $p[3];
      $inside     = $p[4];
    }

    $marker    = "$uniq_prefix-$element-" . sprintf('%08X', $n++) . '-QINU';
    $stripped .= $marker;

    if ($close === '/>') {
      // Empty element tag, <tag />
      $content = NULL;
      $text    = $inside;
      $tail    = NULL;
    } 
    else {
      if ($element == '!--') {
        $end = '/(-->)/';
      }
      else {
        $end = "/(<\\/$element\\s*>)/i";
      }
      $q = preg_split($end, $inside, 2, PREG_SPLIT_DELIM_CAPTURE);
      $content = $q[0];
      if (count($q) < 3) {
        # No end tag -- let it run out to the end of the text.
        $tail = '';
        $text = '';
      } 
      else {
        $tail = $q[1];
        $text = $q[2];
      }
    }

    $matches[$marker] = array(
      $element,
      $content,
      _xml_content_filter_decode_attribs($attributes),
      "<$element$attributes$close$content$tail"
    );
  }
  
  return $stripped;
}
	
	
/**
 * Return an associative array of attribute names and values from a partial
 * tag string. Attribute names are forced to lowercase, character 
 * references are decoded to UTF-8 text. Extracted from MediaWiki.
 *
 * @param $text string
 *  The text to extract attributes from.
 *
 * @return array
 *  An associative array of attribute names as keys and their values.
 */
function _xml_content_filter_decode_attribs($text) {
  $trans   = get_html_translation_table(HTML_ENTITIES);
  $attribs = array();

  if (trim($text) == '') {
    return $attribs;
  }

  $pairs = array();
  if (!preg_match_all(XML_CONTENT_FILTER_ATTRIB_REGEX, $text, $pairs, PREG_SET_ORDER)) {
    return $attribs;
  }

  foreach ($pairs as $set) {
    $attribute = strtolower($set[1]);
    $value     = _xml_content_filter_attrib_callback($set);

    // Normalize whitespace
    $value = preg_replace('/[\t\r\n ]+/', ' ', $value);
    $value = trim($value);

    // Decode character references
    $attribs[$attribute] = strtr($value, $trans);
  }
  
  return $attribs;
}


/**
 * Pick the appropriate attribute value from a match set from the
 * XML_CONTENT_FILTER_ATTRIB_REGEX matches. Extracted from MediaWiki.
 *
 * @param array $set
 *
 * @return string
 *
 */
function _xml_content_filter_attrib_callback($set) {
  if (isset($set[6])) {
    # Illegal #XXXXXX color with no quotes.
    return $set[6];
  }
  elseif (isset($set[5])) {
    # No quotes.
    return $set[5];
  }
  elseif (isset($set[4])) {
    # Single-quoted
    return $set[4];
  }
  elseif (isset($set[3])) {
    # Double-quoted
    return $set[3];
  }
  elseif (!isset($set[2])) {
    # In XHTML, attributes must have a value.
    # For 'reduced' form, return explicitly the attribute name here.
    return $set[1];
  }
  else {
    trigger_error(t('Tag conditions not met. This should never happen and is a bug.'), E_USER_ERROR);
  }
}
